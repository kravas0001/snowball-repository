﻿using System;
using Common;
using Enemy;
using UnityEngine;

namespace Player
{
    public class Player : Character
    {
        [SerializeField] private PlayerSnowball[] _playerSnowballs;
        [SerializeField] private PointsForHitting _pointsForHitting;
        [SerializeField] private float _recharge;

        private bool _hasWon;
        private int _numberOfLives = 3;

        public event EventHandler DiedEventHandler;

        public float Recharge
        {
            get { return _recharge; }
        }

        public bool HasWon
        {
            get { return _hasWon; }
        }

        public int NumberOfLives
        {
            get { return _numberOfLives; }
        }

        private void OnValidate()
        {
            if (_recharge < 1)
                _recharge = 1;
            if (_recharge > 3)
                _recharge = 3;
        }

        private void OnEnable()
        {
            _pointsForHitting.WonEventHandler += Win;
        }

        private void OnDisable()
        {
            _pointsForHitting.WonEventHandler -= Win;
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (!HasWon && isAlive)
            {
                EnemySnowball enemySnowball = other.GetComponent<EnemySnowball>();
                {
                    if (enemySnowball != null && !enemySnowball.InCollision)
                        HandleTriggerCollision(other);
                }
            }
        }

        private void Win(object sender, EventArgs e)
        {
            if (sender is PointsForHitting)
                _hasWon = true;
        }

        protected override void TakeDamage()
        {
            --_numberOfLives;
            if (_numberOfLives == 0)
            {
                isAlive = false;
                if (DiedEventHandler != null)
                    DiedEventHandler.Invoke(this, EventArgs.Empty);
            }
        }

        public void Throw(float throwForceSliderValue)
        {
            for (int i = 0; i < _playerSnowballs.Length; i++)
                if (!_playerSnowballs[i].IsThrown)
                {
                    _playerSnowballs[i].Fly(throwForceSliderValue);
                    break;
                }

            OnThrowingEventHandler();
        }
    }
}