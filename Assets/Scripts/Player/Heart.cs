﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Player
{
    [RequireComponent(typeof(Image))]
    public class Heart : MonoBehaviour
    {
        private enum HeartNumber
        {
            FirstHeart = 1,
            SecondHeart,
            ThirdHear
        }

        [SerializeField] private Player _player;
        [SerializeField] private HeartNumber _heartNumber;

        private Image _image;

        private void Awake()
        {
            _image = GetComponent<Image>();
        }

        private void OnEnable()
        {
            _player.HitEventHandler += ChangeHeartImage;
        }

        private void OnDisable()
        {
            _player.HitEventHandler -= ChangeHeartImage;
        }

        private void ChangeHeartImage(object sender, EventArgs e)
        {
            if (sender is Player && _player.NumberOfLives == Convert.ToInt32(_heartNumber))
                StartCoroutine(ChangeFillAmount());
        }

        private IEnumerator ChangeFillAmount()
        {
            while (_image.fillAmount > 0)
            {
                _image.fillAmount -= Time.deltaTime;
                yield return null;
            }
        }
    }
}