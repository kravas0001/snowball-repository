﻿using System.Collections;
using Common;
using UnityEngine;

namespace Player
{
    public class PlayerSnowball : Snowball
    {
        [SerializeField] private float _shootingAngle = 45;
        [SerializeField] float _gravity = 9.8f;
        [SerializeField] private ForceZone _forceZone;
        [SerializeField] private Transform _leftWallTransform;
        [SerializeField] private Transform _rightWallTransform;
        [SerializeField] private float _minThrowDistance;

        private float _minRequiredForce;

        private void OnValidate()
        {
            if (_shootingAngle < 45)
                _shootingAngle = 45;
            if (_shootingAngle > 60)
                _shootingAngle = 60;
            if (_gravity < 9.8f)
                _gravity = 9.8f;
            if (_gravity > 14.7f)
                _gravity = 14.7f;
            if (_minThrowDistance < 0.5f)
                _minThrowDistance = 0.5f;
            if (_minThrowDistance > 1.5f)
                _minThrowDistance = 1.5f;
        }

        private void Start()
        {
            _minRequiredForce = _forceZone.MinRequiredForceZone;
        }

        public void Fly(float throwForceSliderValue)
        {
            ChangeVisibilityAndThrowStatus(true);
            StartCoroutine(Move(throwForceSliderValue));
        }

        private IEnumerator Move(float throwForceSliderValue)
        {
            yield return waitForCorrectAnimationFrameForThrow;
            myTransform.parent = null;
            float targetPositionX;
            if (throwForceSliderValue >= _minRequiredForce)
                targetPositionX = (_leftWallTransform.position.x + _rightWallTransform.position.x) *
                    throwForceSliderValue / (_minRequiredForce + 1);
            else
                targetPositionX = (myTransform.position.x + _minThrowDistance + _leftWallTransform.position.x) *
                    throwForceSliderValue / _minRequiredForce;
            Vector2 targetPosition = new Vector2(targetPositionX, myTransform.position.y);
            float distance = Vector2.Distance(myTransform.position, targetPosition);
            float velocity = distance / (Mathf.Sin(2 * _shootingAngle * Mathf.Deg2Rad) / _gravity);
            float velocityX = Mathf.Sqrt(velocity) * Mathf.Cos(_shootingAngle * Mathf.Deg2Rad);
            float velocityY = Mathf.Sqrt(velocity) * Mathf.Sin(_shootingAngle * Mathf.Deg2Rad);
            float elapseTime = 0;
            while (CheckVisibilityAndCollision())
            {
                myTransform.Translate(velocityX * Time.deltaTime,
                    (velocityY - _gravity * elapseTime) * Time.deltaTime, 0, Space.World);
                elapseTime += Time.deltaTime;
                yield return null;
            }
        }
    }
}