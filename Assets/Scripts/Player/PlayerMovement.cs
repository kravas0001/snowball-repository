﻿using System;
using System.Collections;
using Animation;
using Animation.Common;
using Common;
using UnityEngine;

namespace Player
{
    [RequireComponent(typeof(JumpAnimation))]
    [RequireComponent(typeof(WinAnimation))]
    [RequireComponent(typeof(DieAnimation))]
    [RequireComponent(typeof(Player))]
    public class PlayerMovement : CharacterMovement, IMovable
    {
        [SerializeField] private float _jumpForce;
        [SerializeField] private ContactFilter2D _groundContactFilter2D;
        [SerializeField] private PointsForHitting _pointsForHitting;

        private Player _player;
        private JumpAnimation _jumpAnimation;
        private WinAnimation _winAnimation;
        private DieAnimation _dieAnimation;
        private float _movement;
        private bool _isGrounded = true;
        private bool _isShooting;

        public bool IsGrounded
        {
            get { return _isGrounded; }
        }

        public event EventHandler<AnimationEventArgs> SettingAnimationEventHandler;

        private void OnValidate()
        {
            ValidateRunSpeed();
            if (_jumpForce < 275)
                _jumpForce = 275;
            if (_jumpForce > 325)
                _jumpForce = 325;
        }

        private void Awake()
        {
            Init();
            _player = GetComponent<Player>();
            _jumpAnimation = GetComponent<JumpAnimation>();
            _dieAnimation = GetComponent<DieAnimation>();
            _winAnimation = GetComponent<WinAnimation>();
            OnSettingAnimationEventHandler(idleAnimation);
        }

        private void OnEnable()
        {
            _player.ThrowingEventHandler += Throw;
            throwAnimation.AnimationCompletedEventHandler += ChangeThrowStatus;
            _player.DiedEventHandler += Die;
            _jumpAnimation.AnimationCompletedEventHandler += CheckGround;
            _pointsForHitting.WonEventHandler += Win;
        }

        private void OnDisable()
        {
            _player.ThrowingEventHandler -= Throw;
            throwAnimation.AnimationCompletedEventHandler -= ChangeThrowStatus;
            _player.DiedEventHandler -= Die;
            _jumpAnimation.AnimationCompletedEventHandler -= CheckGround;
            _pointsForHitting.WonEventHandler -= Win;
        }

        private void Update()
        {
            if (_player.IsAlive && !_isShooting && !_player.HasWon)
            {
                if (SimpleInput.GetButtonDown("Jump") && _isGrounded)
                    Jump();

                _movement = SimpleInput.GetAxis("Horizontal");
                if (_movement <= -0.25f || _movement >= 0.25f)
                    Run();
                else
                {
                    OnSettingAnimationEventHandler(idleAnimation);
                    if (moveCoroutine != null)
                    {
                        StopCoroutine(moveCoroutine);
                        moveCoroutine = null;
                    }
                }
            }
        }

        private void Throw(object sender, EventArgs e)
        {
            if (sender is Player)
            {
                _isShooting = true;
                if (moveCoroutine != null)
                {
                    StopCoroutine(moveCoroutine);
                    moveCoroutine = null;
                }

                myTransform.localScale = Vector2.one;
                OnSettingAnimationEventHandler(throwAnimation);
            }
        }

        private void ChangeThrowStatus(object sender, EventArgs e)
        {
            if (sender is ThrowAnimation)
                _isShooting = false;
        }

        private void CheckGround(object sender, EventArgs e)
        {
            if (sender is JumpAnimation)
                StartCoroutine(ThrowRaycast());
        }

        private void Win(object sender, EventArgs e)
        {
            if (sender is PointsForHitting)
            {
                _jumpAnimation.AnimationCompletedEventHandler -= CheckGround;
                if (moveCoroutine != null)
                    StopCoroutine(moveCoroutine);
                OnSettingAnimationEventHandler(_winAnimation);
            }
        }

        private void Jump()
        {
            OnSettingAnimationEventHandler(_jumpAnimation);
            rigidbody2D.AddForce(Vector2.up * _jumpForce, ForceMode2D.Force);
            _isGrounded = false;
        }

        private void Run()
        {
            OnSettingAnimationEventHandler(runAnimation);
            if (moveCoroutine == null)
                moveCoroutine = StartCoroutine(Move());
            myTransform.localScale = _movement > 0 ? Vector2.one : new Vector2(-1, 1);
        }

        private IEnumerator Move()
        {
            WaitForFixedUpdate waitForFixedUpdate = new WaitForFixedUpdate();
            while (true)
            {
                rigidbody2D.velocity =
                    new Vector2(_movement * runSpeed * Time.fixedDeltaTime, rigidbody2D.velocity.y);
                yield return waitForFixedUpdate;
            }
        }

        private IEnumerator ThrowRaycast()
        {
            WaitForFixedUpdate waitForFixedUpdate = new WaitForFixedUpdate();
            RaycastHit2D[] raycastHit2Ds = new RaycastHit2D[1];
            while (true)
            {
                int result = Physics2D.Raycast(myTransform.position, Vector2.down, _groundContactFilter2D,
                    raycastHit2Ds, 0.05f);
                if (result != 0)
                {
                    _isGrounded = true;
                    if (_player.IsAlive && !_player.HasWon)
                        OnSettingAnimationEventHandler(idleAnimation);
                    break;
                }

                yield return waitForFixedUpdate;
            }
        }

        private void Die(object sender, EventArgs e)
        {
            if (sender is Player)
            {
                _jumpAnimation.AnimationCompletedEventHandler -= CheckGround;
                if (moveCoroutine != null)
                    StopCoroutine(moveCoroutine);
                OnSettingAnimationEventHandler(_dieAnimation);
            }
        }

        protected virtual void OnSettingAnimationEventHandler(IAnimated animated)
        {
            if (SettingAnimationEventHandler != null &&
                (animated is DieAnimation || animated is WinAnimation || _isGrounded))
                SettingAnimationEventHandler.Invoke(this, new AnimationEventArgs(animated));
        }
    }
}