﻿using System;
using Animation.Common;
using UnityEngine;

public class Panel : MonoBehaviour
{
    [SerializeField] private AnimationRequirement _animationRequirement;
    [SerializeField] private GameObject _panelGameObject;

    private void OnEnable()
    {
        _animationRequirement.AnimationCompletedEventHandler += SetActivePanel;
    }

    private void OnDisable()
    {
        _animationRequirement.AnimationCompletedEventHandler -= SetActivePanel;
    }

    private void SetActivePanel(object sender, EventArgs e)
    {
        if (sender is AnimationRequirement)
        {
            Time.timeScale = 0;
            _panelGameObject.SetActive(true);
        }
    }
}