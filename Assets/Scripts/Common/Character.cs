﻿using System;
using UnityEngine;

namespace Common
{
    public abstract class Character : MonoBehaviour
    {
        protected bool isAlive = true;

        public bool IsAlive
        {
            get { return isAlive; }
        }

        public event EventHandler HitEventHandler;
        public event EventHandler ThrowingEventHandler;

        protected virtual void OnHitEventHandler()
        {
            if (HitEventHandler != null)
                HitEventHandler.Invoke(this, EventArgs.Empty);
        }

        protected virtual void OnThrowingEventHandler()
        {
            if (ThrowingEventHandler != null)
                ThrowingEventHandler.Invoke(this, EventArgs.Empty);
        }

        protected void HandleTriggerCollision(Collider2D other)
        {
            other.GetComponent<Snowball>().ResetAndStartParticle();
            OnHitEventHandler();
            TakeDamage();
        }

        protected abstract void TakeDamage();
    }
}