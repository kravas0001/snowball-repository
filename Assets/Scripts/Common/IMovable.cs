﻿using System;
using Animation.Common;

namespace Common
{
    public interface IMovable
    {
        event EventHandler<AnimationEventArgs> SettingAnimationEventHandler;
    }
}