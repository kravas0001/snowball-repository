﻿using System.Collections;
using Animation;
using UnityEngine;

namespace Common
{
    [RequireComponent(typeof(SpriteRenderer))]
    public abstract class Snowball : MonoBehaviour
    {
        [SerializeField] private ThrowAnimation _throwAnimation;

        private ParticleSystem _particleSystem;
        private SpriteRenderer _spriteRenderer;
        private Camera _camera;
        private Transform _parentTransform;
        private bool _inCollision;
        private bool _isThrown;

        protected Transform myTransform;
        protected WaitForSeconds waitForCorrectAnimationFrameForThrow;

        public bool InCollision
        {
            get { return _inCollision; }
        }

        public bool IsThrown
        {
            get { return _isThrown; }
        }

        private void Awake()
        {
            _particleSystem = GetComponentInChildren<ParticleSystem>();
            _spriteRenderer = GetComponent<SpriteRenderer>();
            ChangeVisibilityAndThrowStatus(false);
            _camera = Camera.main;
            myTransform = transform;
            _parentTransform = myTransform.parent;
            waitForCorrectAnimationFrameForThrow = new WaitForSeconds(_throwAnimation.Duration * 0.8f);
        }

        private void HardReset()
        {
            _inCollision = false;
            ChangeVisibilityAndThrowStatus(false);
            myTransform.parent = _parentTransform;
            myTransform.localPosition = Vector2.zero;
        }

        protected void ChangeVisibilityAndThrowStatus(bool visibilityAndThrowStatus)
        {
            _spriteRenderer.enabled = _isThrown = visibilityAndThrowStatus;
        }

        protected bool CheckVisibilityAndCollision()
        {
            Vector3 position = _camera.WorldToViewportPoint(myTransform.position);
            if (!_inCollision && position.z > 0 && position.x > 0 && position.x < 1 && position.y > 0 && position.y < 1)
                return true;
            HardReset();
            return false;
        }

        public void ResetAndStartParticle()
        {
            _inCollision = true;
            StartCoroutine(PlayParticleSystem());
        }

        private IEnumerator PlayParticleSystem()
        {
            _particleSystem.transform.parent = null;
            _particleSystem.Play();
            yield return new WaitForSeconds(
                _particleSystem.main.duration + _particleSystem.main.startLifetimeMultiplier);
            _particleSystem.transform.parent = myTransform;
            _particleSystem.transform.localPosition = Vector2.zero;
        }
    }
}