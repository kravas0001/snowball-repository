﻿using System;
using Animation;
using UnityEngine;

namespace Common
{
    [RequireComponent(typeof(Rigidbody2D))]
    [RequireComponent(typeof(IdleAnimation))]
    [RequireComponent(typeof(RunAnimation))]
    [RequireComponent(typeof(ThrowAnimation))]
    public abstract class CharacterMovement : MonoBehaviour
    {
        [SerializeField] protected float runSpeed;

        protected Transform myTransform;
        protected new Rigidbody2D rigidbody2D;
        protected IdleAnimation idleAnimation;
        protected RunAnimation runAnimation;
        protected ThrowAnimation throwAnimation;
        protected Coroutine moveCoroutine;

        protected void ValidateRunSpeed()
        {
            if (runSpeed < 125)
                runSpeed = 125;
            if (runSpeed > 175)
                runSpeed = 175;
        }

        protected void Init()
        {
            myTransform = transform;
            rigidbody2D = GetComponent<Rigidbody2D>();
            idleAnimation = GetComponent<IdleAnimation>();
            runAnimation = GetComponent<RunAnimation>();
            throwAnimation = GetComponent<ThrowAnimation>();
        }
    }
}