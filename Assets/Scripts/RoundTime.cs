﻿using System.Collections;
using TMPro;
using UnityEngine;

[RequireComponent(typeof(TMP_Text))]
public class RoundTime : MonoBehaviour
{
    private TMP_Text _text;
    private int _seconds;
    private int _minutes;

    private void Start()
    {
        _text = GetComponent<TMP_Text>();
        PrintRoundTime();
        StartCoroutine(CountingTime());
    }

    private IEnumerator CountingTime()
    {
        WaitForSeconds waitForSeconds = new WaitForSeconds(1);
        while (true)
        {
            yield return waitForSeconds;
            ++_seconds;
            if (_seconds == 60)
            {
                ++_minutes;
                _seconds = 0;
            }

            PrintRoundTime();
        }
    }

    private void PrintRoundTime()
    {
        _text.text = string.Format("{0}:{1}",
            _minutes < 10 ? string.Concat("0", _minutes.ToString()) : _minutes.ToString(),
            _seconds < 10 ? string.Concat("0", _seconds.ToString()) : _seconds.ToString());
    }
}