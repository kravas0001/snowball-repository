﻿using UnityEngine;

public class StarsNumber : MonoBehaviour
{
    [SerializeField] private Player.Player _player;

    private void Awake()
    {
        for (int i = 0; i < _player.NumberOfLives; i++)
            transform.GetChild(i).gameObject.SetActive(true);
    }
}