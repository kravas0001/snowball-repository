﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Slider))]
public class ThrowForceSlider : MonoBehaviour
{
    [SerializeField] private float _speed;
    
    private Slider _slider;

    private void OnValidate()
    {
        if (_speed < 0.5f)
            _speed = 0.5f;
        if (_speed > 1.5f)
            _speed = 1.5f;
    }

    private void Awake()
    {
        _slider = GetComponent<Slider>();
    }

    private void Update()
    {
        if (_slider.value + Time.deltaTime * _speed < _slider.maxValue)
            _slider.value += Time.deltaTime * _speed;
        else
            _slider.value = _slider.minValue;
    }
}