﻿using System;
using TMPro;
using UnityEngine;

[RequireComponent(typeof(TMP_Text))]
public class PointsForHitting : MonoBehaviour
{
    [SerializeField] private int _requiredPointsToWin;
    [SerializeField] private Player.Player _player;
    [SerializeField] private Enemy.Enemy[] _enemies;

    private TMP_Text _text;
    private int _currentPoints;

    public event EventHandler WonEventHandler;

    private void OnValidate()
    {
        if (_requiredPointsToWin < 1)
            _requiredPointsToWin = 1;
    }

    private void Awake()
    {
        _text = GetComponent<TMP_Text>();
        ChangeEarnedPointsText();
    }

    private void OnEnable()
    {
        for (int i = 0; i < _enemies.Length; i++)
            _enemies[i].HitEventHandler += AddPoints;
    }

    private void OnDisable()
    {
        for (int i = 0; i < _enemies.Length; i++)
            _enemies[i].HitEventHandler -= AddPoints;
    }

    private void AddPoints(object sender, EventArgs e)
    {
        Enemy.Enemy enemy = sender as Enemy.Enemy;
        if (enemy != null && _player.IsAlive)
        {
            _currentPoints += enemy.PointsPerHit;
            ChangeEarnedPointsText();
            if (_currentPoints >= _requiredPointsToWin)
                OnWonEventHandler();
        }
    }

    protected virtual void OnWonEventHandler()
    {
        if (WonEventHandler != null)
            WonEventHandler.Invoke(this, EventArgs.Empty);
    }

    private void ChangeEarnedPointsText()
    {
        _text.text = String.Format("{0}/{1}", _currentPoints, _requiredPointsToWin);
    }
}