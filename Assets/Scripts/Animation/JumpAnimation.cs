using Animation.Common;
using Spine.Unity;

namespace Animation
{
    public class JumpAnimation : AnimationRequirement, IAnimated
    {
        public CharacterAnimation.CharacterState CharacterState
        {
            get { return CharacterAnimation.CharacterState.Jump; }
        }

        public void Animate(SkeletonAnimation skeletonAnimation)
        {
            Animate(skeletonAnimation, false);
        }
    }
}