﻿using System;

namespace Animation.Common
{
    public class AnimationEventArgs : EventArgs
    {
        private readonly IAnimated _animated;

        public IAnimated Animated
        {
            get { return _animated; }
        }

        public AnimationEventArgs(IAnimated animated)
        {
            _animated = animated;
        }
    }
}