using Common;
using Spine.Unity;
using UnityEngine;

namespace Animation.Common
{
    [RequireComponent(typeof(IMovable), typeof(SkeletonAnimation))]
    public class CharacterAnimation : MonoBehaviour
    {
        public enum CharacterState
        {
            Win,
            Die,
            Throw,
            Jump,
            Run,
            Idle
        }

        private IMovable _movable;
        private SkeletonAnimation _skeletonAnimation;
        private CharacterState _previousCharacterState;
        private CharacterState _currentCharacterState;

        private void Awake()
        {
            _movable = GetComponent<IMovable>();
            _skeletonAnimation = GetComponent<SkeletonAnimation>();
        }

        private void OnEnable()
        {
            _movable.SettingAnimationEventHandler += SetAnimation;
        }

        private void OnDisable()
        {
            _movable.SettingAnimationEventHandler -= SetAnimation;
        }

        private void SetAnimation(object sender, AnimationEventArgs e)
        {
            if (sender is IMovable)
            {
                _previousCharacterState = _currentCharacterState;
                _currentCharacterState = e.Animated.CharacterState;
                if (_previousCharacterState != _currentCharacterState)
                    e.Animated.Animate(_skeletonAnimation);
            }
        }
    }
}