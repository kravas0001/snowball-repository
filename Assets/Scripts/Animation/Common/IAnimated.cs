﻿using Spine.Unity;

namespace Animation.Common
{
    public interface IAnimated
    {
        CharacterAnimation.CharacterState CharacterState { get; }
        void Animate(SkeletonAnimation skeletonAnimation);
    }
}
