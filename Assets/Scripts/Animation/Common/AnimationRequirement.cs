﻿using System;
using Spine;
using Spine.Unity;
using UnityEngine;

namespace Animation.Common
{
    public abstract class AnimationRequirement : MonoBehaviour
    {
        [SerializeField] protected AnimationReferenceAsset animationReferenceAsset;
        [SerializeField] protected float timeScale;

        public event EventHandler AnimationCompletedEventHandler;

        private void OnValidate()
        {
            if (timeScale < 0.5f)
                timeScale = 0.5f;
            if (timeScale > 1.5f)
                timeScale = 1.5f;
        }

        protected void Animate(SkeletonAnimation skeletonAnimation, bool loop)
        {
            TrackEntry trackEntry = skeletonAnimation.state.SetAnimation(0, animationReferenceAsset, loop);
            trackEntry.timeScale = timeScale;
            if (!loop)
                trackEntry.Complete += TrackEntryOnComplete;
        }

        private void TrackEntryOnComplete(TrackEntry trackEntry)
        {
            OnAnimationCompletedEventHandler(trackEntry);
        }

        protected virtual void OnAnimationCompletedEventHandler(TrackEntry trackEntry)
        {
            if (AnimationCompletedEventHandler != null)
                AnimationCompletedEventHandler.Invoke(this, EventArgs.Empty);
            trackEntry.Complete -= TrackEntryOnComplete;
        }
    }
}