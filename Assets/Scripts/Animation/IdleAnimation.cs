﻿using Animation.Common;
using Spine.Unity;

namespace Animation
{
    public class IdleAnimation : AnimationRequirement, IAnimated
    {
        public CharacterAnimation.CharacterState CharacterState
        {
            get { return CharacterAnimation.CharacterState.Idle; }
        }

        public void Animate(SkeletonAnimation skeletonAnimation)
        {
            Animate(skeletonAnimation, true);
        }
    }
}