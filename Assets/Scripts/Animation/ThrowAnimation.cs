﻿using Animation.Common;
using Spine.Unity;

namespace Animation
{
    public class ThrowAnimation : AnimationRequirement, IAnimated
    {
        public float Duration
        {
            get { return animationReferenceAsset.Animation.Duration / timeScale; }
        }

        public CharacterAnimation.CharacterState CharacterState
        {
            get { return CharacterAnimation.CharacterState.Throw; }
        }

        public void Animate(SkeletonAnimation skeletonAnimation)
        {
            Animate(skeletonAnimation, false);
        }
    }
}