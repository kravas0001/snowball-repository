﻿using Animation.Common;
using Spine.Unity;

namespace Animation
{
    public class WinAnimation : AnimationRequirement, IAnimated
    {
        public CharacterAnimation.CharacterState CharacterState
        {
            get { return CharacterAnimation.CharacterState.Win; }
        }

        public void Animate(SkeletonAnimation skeletonAnimation)
        {
            Animate(skeletonAnimation, false);
        }
    }
}