using Animation.Common;
using Spine.Unity;

namespace Animation
{
    public class RunAnimation : AnimationRequirement, IAnimated
    {
        public CharacterAnimation.CharacterState CharacterState
        {
            get { return CharacterAnimation.CharacterState.Run; }
        }

        public void Animate(SkeletonAnimation skeletonAnimation)
        {
            Animate(skeletonAnimation, true);
        }
    }
}