﻿using Animation.Common;
using Spine.Unity;

namespace Animation
{
    public class DieAnimation : AnimationRequirement, IAnimated
    {
        public CharacterAnimation.CharacterState CharacterState
        {
            get { return CharacterAnimation.CharacterState.Die; }
        }

        public void Animate(SkeletonAnimation skeletonAnimation)
        {
            Animate(skeletonAnimation, false);
        }
    }
}