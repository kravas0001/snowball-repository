﻿using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Enemy
{
    public class EnemyThrowTimer : MonoBehaviour
    {
        [SerializeField] private float _throwsFrequency;

        private Enemy[] _enemies;

        private void OnValidate()
        {
            if (_throwsFrequency < 3)
                _throwsFrequency = 3;
            if (_throwsFrequency > 7)
                _throwsFrequency = 7;
        }

        private void Start()
        {
            _enemies = GetComponentsInChildren<Enemy>();
            StartCoroutine(CountdownToThrow());
        }

        private IEnumerator CountdownToThrow()
        {
            WaitForSeconds waitForSecondsToShoot = new WaitForSeconds(_throwsFrequency);
            while (true)
            {
                yield return waitForSecondsToShoot;
                while (true)
                {
                    int indexOfRandomShooter = Random.Range(0, _enemies.Length);
                    if (_enemies[indexOfRandomShooter].IsAlive)
                    {
                        _enemies[indexOfRandomShooter].Throw();
                        break;
                    }

                    yield return null;
                }
            }
        }
    }
}