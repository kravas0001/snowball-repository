﻿using System.Collections;
using Common;
using UnityEngine;

namespace Enemy
{
    public class EnemySnowball : Snowball
    {
        [SerializeField] private Transform _playerTransform;
        [SerializeField] private float _speed;

        private void OnValidate()
        {
            if (_speed < 0.75f)
                _speed = 0.75f;
            if (_speed > 1.25f)
                _speed = 1.25f;
        }

        public void Fly()
        {
            ChangeVisibilityAndThrowStatus(true);
            StartCoroutine(Move());
        }

        private IEnumerator Move()
        {
            yield return waitForCorrectAnimationFrameForThrow;
            myTransform.parent = null;
            Vector2 direction = _playerTransform.position - myTransform.position;
            while (CheckVisibilityAndCollision())
            {
                myTransform.Translate(direction * (_speed * Time.deltaTime), Space.World);
                yield return null;
            }
        }
    }
}