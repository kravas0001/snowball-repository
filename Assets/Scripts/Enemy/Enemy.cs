﻿using Common;
using Player;
using UnityEngine;

namespace Enemy
{
    public class Enemy : Character
    {
        [SerializeField] private EnemySnowball[] _enemySnowballs;
        [SerializeField] private int _pointsPerHit;

        public int PointsPerHit
        {
            get { return _pointsPerHit; }
        }

        private void OnValidate()
        {
            if (_pointsPerHit < 1) 
                _pointsPerHit = 1;
            if (_pointsPerHit > 3)
                _pointsPerHit = 3;
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (isAlive)
            {
                PlayerSnowball playerSnowball = other.GetComponent<PlayerSnowball>();
                if (playerSnowball != null && !playerSnowball.InCollision)
                    HandleTriggerCollision(other);
            }
        }

        protected override void TakeDamage()
        {
            isAlive = false;
        }

        public void Recover()
        {
            isAlive = true;
        }

        public void Throw()
        {
            for (int i = 0; i < _enemySnowballs.Length; i++)
                if (!_enemySnowballs[i].IsThrown)
                {
                    _enemySnowballs[i].Fly();
                    break;
                }

            OnThrowingEventHandler();
        }
    }
}