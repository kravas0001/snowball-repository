﻿using System;
using System.Collections;
using Animation;
using Animation.Common;
using Common;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Enemy
{
    [RequireComponent(typeof(Enemy))]
    public class EnemyMovement : CharacterMovement, IMovable
    {
        [SerializeField] private float _startDelay;
        [SerializeField] private float _idleFrequency;
        [SerializeField] private float _idleFrequencyDeltaTime;
        [SerializeField] private float _idleDuration;
        [SerializeField] private float _idleDurationDeltaTime;
        [SerializeField] private ContactFilter2D _wallContactFilter2D;

        private Enemy _enemy;
        private int _lookDirection = -1;
        private bool _needCheckWall;
        private bool _isShooting;
        private float _startPositionX;

        public event EventHandler<AnimationEventArgs> SettingAnimationEventHandler;

        private void OnValidate()
        {
            ValidateRunSpeed();
            if (_startDelay < 0)
                _startDelay = 0;
            if (_startDelay > 2)
                _startDelay = 2;
            if (_idleFrequency < 5)
                _idleFrequency = 5;
            if (_idleFrequency > 15)
                _idleFrequency = 15;
            if (_idleFrequencyDeltaTime < 1)
                _idleFrequencyDeltaTime = 1;
            if (_idleFrequencyDeltaTime > 3)
                _idleFrequencyDeltaTime = 3;
            if (_idleDuration < 1.5f)
                _idleDuration = 1.5f;
            if (_idleDuration > 2.5f)
                _idleDuration = 2.5f;
            if (_idleDurationDeltaTime < 0.3f)
                _idleDurationDeltaTime = 0.3f;
            if (_idleDurationDeltaTime > 0.5f)
                _idleDurationDeltaTime = 0.5f;
        }

        private void Awake()
        {
            Init();
            _enemy = GetComponent<Enemy>();
            moveCoroutine = StartCoroutine(Move(_startDelay));
            _startPositionX = myTransform.position.x;
        }

        private void OnEnable()
        {
            _enemy.ThrowingEventHandler += Throw;
            throwAnimation.AnimationCompletedEventHandler += Run;
            _enemy.HitEventHandler += RunAway;
        }

        private void OnDisable()
        {
            _enemy.ThrowingEventHandler -= Throw;
            throwAnimation.AnimationCompletedEventHandler -= Run;
            _enemy.HitEventHandler -= RunAway;
        }

        private void Throw(object sender, EventArgs e)
        {
            if (sender is Enemy)
            {
                _isShooting = true;
                StopCoroutine(moveCoroutine);
                moveCoroutine = null;
                if (_lookDirection == 1)
                    myTransform.localScale = new Vector2(-1, 1);
                OnSettingAnimationEventHandler(throwAnimation);
            }
        }

        private void Run(object sender, EventArgs e)
        {
            if (sender is ThrowAnimation)
            {
                _isShooting = false;
                if (!_enemy.IsAlive)
                {
                    StartCoroutine(MoveAway());
                    return;
                }

                if (_lookDirection == 1)
                    myTransform.localScale = Vector2.one;
                moveCoroutine = StartCoroutine(Move(0));
            }
        }

        private void RunAway(object sender, EventArgs e)
        {
            if (sender is Enemy)
            {
                if (_isShooting)
                    return;
                if (moveCoroutine != null)
                {
                    StopCoroutine(moveCoroutine);
                    moveCoroutine = null;
                }

                StartCoroutine(MoveAway());
            }
        }

        private IEnumerator Move(float startDelay)
        {
            yield return new WaitForSeconds(startDelay);
            WaitForFixedUpdate waitForFixedUpdate = new WaitForFixedUpdate();
            float elapsedTime = 0;
            while (true)
            {
                OnSettingAnimationEventHandler(runAnimation);
                float randomIdleFrequency = Random.Range(_idleFrequency, _idleFrequency + _idleFrequencyDeltaTime);
                while (elapsedTime < randomIdleFrequency)
                {
                    rigidbody2D.velocity =
                        new Vector2(_lookDirection * runSpeed * Time.fixedDeltaTime, rigidbody2D.velocity.y);
                    yield return waitForFixedUpdate;
                    CheckWall();
                    elapsedTime += Time.fixedDeltaTime;
                }

                elapsedTime = 0;
                OnSettingAnimationEventHandler(idleAnimation);
                yield return new WaitForSeconds(Random.Range(_idleDuration, _idleDuration + _idleDurationDeltaTime));
            }
        }

        private IEnumerator MoveAway()
        {
            myTransform.localScale = Vector2.one;
            OnSettingAnimationEventHandler(runAnimation);
            WaitForFixedUpdate waitForFixedUpdate = new WaitForFixedUpdate();
            while (_startPositionX - myTransform.position.x > 0.01f)
            {
                rigidbody2D.velocity =
                    new Vector2(runSpeed * Time.fixedDeltaTime, rigidbody2D.velocity.y);
                yield return waitForFixedUpdate;
            }

            _needCheckWall = false;
            _lookDirection = -1;
            myTransform.localScale = new Vector2(-1, 1);
            _enemy.Recover();
            moveCoroutine = StartCoroutine(Move(_startDelay));
        }

        private void CheckWall()
        {
            RaycastHit2D[] raycastHit2D = new RaycastHit2D[1];
            int result = Physics2D.Raycast(myTransform.position, Vector2.right * _lookDirection, _wallContactFilter2D,
                raycastHit2D, 0.8f);
            if (result != 0)
            {
                if (raycastHit2D[0].transform.GetComponent<RightWall>() != null && !_needCheckWall)
                    return;
                _lookDirection *= -1;
                myTransform.localScale = new Vector2(_lookDirection, 1);
                _needCheckWall = true;
            }
        }

        protected virtual void OnSettingAnimationEventHandler(IAnimated animated)
        {
            if (SettingAnimationEventHandler != null)
                SettingAnimationEventHandler.Invoke(this, new AnimationEventArgs(animated));
        }
    }
}