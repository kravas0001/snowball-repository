﻿using UnityEngine;
using UnityEngine.UI;

namespace Background
{
    [RequireComponent(typeof(RawImage))]
    public class Parallax : MonoBehaviour
    {
        [SerializeField] private float _speed;

        private RawImage _rawImage;
        private float _currentPositionX;

        private void OnValidate()
        {
            if (_speed < 0.0025f)
                _speed = 0.0025f;
            if (_speed > 0.0075f)
                _speed = 0.0075f;
        }

        private void Start()
        {
            _rawImage = GetComponent<RawImage>();
            _currentPositionX = _rawImage.uvRect.x;
        }

        private void Update()
        {
            _currentPositionX += _speed * Time.deltaTime;
            Rect uvRect = _rawImage.uvRect;
            _rawImage.uvRect = new Rect(_currentPositionX, uvRect.y, uvRect.width, uvRect.height);
        }
    }
}