﻿using System;
using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Background
{
    public class Cloud : MonoBehaviour
    {
        [SerializeField] private float _minStartDelay;
        [SerializeField] private float _maxStartDelay;

        public event EventHandler MowingEventHandler;

        private void OnValidate()
        {
            if (_minStartDelay < 0)
                _minStartDelay = 0;
            if (_maxStartDelay < 0)
                _maxStartDelay = 0;
            if (_minStartDelay > 10)
                _minStartDelay = 10;
            if (_maxStartDelay > 10)
                _maxStartDelay = 10;
            if (_minStartDelay > _maxStartDelay)
            {
                float temporary = _minStartDelay;
                _minStartDelay = _maxStartDelay;
                _maxStartDelay = temporary;
            }
        }

        private void Start()
        {
            StartCoroutine(SetStartDelay());
        }

        private IEnumerator SetStartDelay()
        {
            yield return new WaitForSeconds(Random.Range(_minStartDelay, _maxStartDelay));
            OnMowingEventHandler();
        }

        protected virtual void OnMowingEventHandler()
        {
            if (MowingEventHandler != null)
                MowingEventHandler.Invoke(this, EventArgs.Empty);
        }
    }
}