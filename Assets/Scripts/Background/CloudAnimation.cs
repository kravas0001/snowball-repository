﻿using UnityEngine;

namespace Background
{
    [RequireComponent(typeof(Animator))]
    public class CloudAnimation : CloudChanger
    {
        [SerializeField] private float _speed;

        private Animator _animator;
        private readonly string _moveParameter = "Move";
        private readonly string _speedMultiplierParameter = "Speed";

        private void OnValidate()
        {
            if (_speed < 0.01f)
                _speed = 0.01f;
            if (_speed > 0.03f)
                _speed = 0.03f;
        }

        private void Start()
        {
            _animator = GetComponent<Animator>();
        }

        protected override void ChangeCloud()
        {
            _animator.SetFloat(_speedMultiplierParameter, _speed);
            _animator.SetTrigger(_moveParameter);
            cloud.MowingEventHandler -= TryChangeCloud;
        }
    }
}