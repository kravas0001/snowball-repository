﻿using UnityEngine;
using Random = UnityEngine.Random;

namespace Background
{
    public class CloudScale : CloudChanger
    {
        [SerializeField] private float _deltaScale;

        private Transform _transform;
        private Vector3 _startScale;

        private void OnValidate()
        {
            if (_deltaScale < 0)
                _deltaScale = 0;
            if (_deltaScale > 0.5f)
                _deltaScale = 0.5f;
        }

        private void Start()
        {
            _transform = transform;
            _startScale = _transform.localScale;
        }

        protected override void ChangeCloud()
        {
            float randomScale = Random.Range(0, _deltaScale);
            _transform.localScale = new Vector3(_startScale.x + randomScale, _startScale.y + randomScale);
        }
    }
}