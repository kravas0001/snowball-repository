﻿using System;
using UnityEngine;

namespace Background
{
    [RequireComponent(typeof(Cloud))]
    public abstract class CloudChanger : MonoBehaviour
    {
        protected Cloud cloud;

        private void Awake()
        {
            cloud = GetComponent<Cloud>();
        }

        private void OnEnable()
        {
            cloud.MowingEventHandler += TryChangeCloud;
        }

        private void OnDisable()
        {
            cloud.MowingEventHandler -= TryChangeCloud;
        }

        protected void TryChangeCloud(object sender, EventArgs e)
        {
            if (sender is Cloud)
                ChangeCloud();
        }

        protected abstract void ChangeCloud();
    }
}