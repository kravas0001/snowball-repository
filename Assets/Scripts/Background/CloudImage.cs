﻿using UnityEngine;
using Random = UnityEngine.Random;

namespace Background
{
    [RequireComponent(typeof(SpriteRenderer))]
    public class CloudImage : CloudChanger
    {
        [SerializeField] private Sprite[] _sprites;

        private SpriteRenderer _spriteRenderer;

        private void Start()
        {
            _spriteRenderer = GetComponent<SpriteRenderer>();
        }

        protected override void ChangeCloud()
        {
            if (_sprites != null)
                _spriteRenderer.sprite = _sprites[Random.Range(0, _sprites.Length)];
        }
    }
}