﻿using UnityEngine;

namespace Button
{
    [RequireComponent(typeof(UnityEngine.UI.Button))]
    public class QuitButton : MonoBehaviour
    {
        private UnityEngine.UI.Button _button;

        private void Awake()
        {
            _button = GetComponent<UnityEngine.UI.Button>();
        }

        private void OnEnable()
        {
            _button.onClick.AddListener(QuitApplication);
        }

        private void OnDisable()
        {
            _button.onClick.RemoveListener(QuitApplication);
        }

        private void QuitApplication()
        {
            Application.Quit();
        }
    }
}