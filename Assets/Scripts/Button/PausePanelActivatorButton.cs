﻿using UnityEngine;

namespace Button
{
    [RequireComponent(typeof(UnityEngine.UI.Button))]
    public class PausePanelActivatorButton : MonoBehaviour
    {
        private enum Action
        {
            Pause,
            Continue
        }

        [SerializeField] private GameObject _pausePanel;
        [SerializeField] private Action _action;

        private UnityEngine.UI.Button _button;

        private void Awake()
        {
            _button = GetComponent<UnityEngine.UI.Button>();
        }

        private void OnEnable()
        {
            _button.onClick.AddListener(() => SetActivePausePanel(_action));
        }

        private void OnDisable()
        {
            _button.onClick.RemoveListener(() => SetActivePausePanel(_action));
        }

        private void SetActivePausePanel(Action action)
        {
            if (action == Action.Pause)
            {
                Time.timeScale = 0;
                _pausePanel.SetActive(true);
            }
            else
            {
                Time.timeScale = 1;
                _pausePanel.SetActive(false);
            }
        }
    }
}