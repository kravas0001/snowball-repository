﻿using System.Collections;
using Player;
using UnityEngine;
using UnityEngine.UI;

namespace Button
{
    [RequireComponent(typeof(UnityEngine.UI.Button), typeof(Image))]
    public class ThrowButton : MonoBehaviour
    {
        [SerializeField] private Player.Player _player;
        [SerializeField] private Slider _slider;

        private PlayerMovement _playerMovement;
        private UnityEngine.UI.Button _button;
        private Image _image;
        private float _recharge;

        private void Awake()
        {
            _playerMovement = _player.GetComponent<PlayerMovement>();
            _button = GetComponent<UnityEngine.UI.Button>();
            _image = GetComponent<Image>();
            _recharge = _player.Recharge;
        }

        private void OnEnable()
        {
            _button.onClick.AddListener(ThrowSnowball);
        }

        private void OnDisable()
        {
            _button.onClick.RemoveListener(ThrowSnowball);
        }

        private void ThrowSnowball()
        {
            if (_playerMovement.IsGrounded && _player.IsAlive && !_player.HasWon)
            {
                _player.Throw(_slider.value);
                StartCoroutine(CountdownToThrow());
            }
        }

        private IEnumerator CountdownToThrow()
        {
            _button.interactable = false;
            while (_image.fillAmount > 0)
            {
                _image.fillAmount -= Time.deltaTime / _recharge * 2;
                yield return null;
            }

            while (_image.fillAmount < 1)
            {
                _image.fillAmount += Time.deltaTime / _recharge * 2;
                yield return null;
            }

            _button.interactable = true;
        }
    }
}