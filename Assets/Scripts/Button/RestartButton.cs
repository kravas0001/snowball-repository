﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Button
{
    [RequireComponent(typeof(UnityEngine.UI.Button))]
    public class RestartButton : MonoBehaviour
    {
        private UnityEngine.UI.Button _button;

        private void Awake()
        {
            _button = GetComponent<UnityEngine.UI.Button>();
        }

        private void OnEnable()
        {
            _button.onClick.AddListener(LoadStartScene);
        }

        private void OnDisable()
        {
            _button.onClick.RemoveListener(LoadStartScene);
        }

        private void LoadStartScene()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            Time.timeScale = 1;
        }
    }
}