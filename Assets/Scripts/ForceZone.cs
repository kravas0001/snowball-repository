﻿using UnityEngine;

[RequireComponent(typeof(RectTransform))]
public class ForceZone : MonoBehaviour
{
    [SerializeField] private RectTransform _leftForceZone;
    [SerializeField] private RectTransform _rightForceZone;
    [SerializeField] private int _minRequiredForceZoneInPercentages;

    private RectTransform _rectTransform;
    private float _interval = 2.5f;

    public float MinRequiredForceZone
    {
        get { return 1 - (float) _minRequiredForceZoneInPercentages / 100; }
    }

    private void OnValidate()
    {
        if (_minRequiredForceZoneInPercentages < 10)
            _minRequiredForceZoneInPercentages = 10;
        if (_minRequiredForceZoneInPercentages > 30)
            _minRequiredForceZoneInPercentages = 30;
    }

    private void Awake()
    {
        _rectTransform = GetComponent<RectTransform>();
        float width = _rectTransform.rect.width;
        _rightForceZone.offsetMin =
            new Vector2(width * MinRequiredForceZone, _rightForceZone.offsetMin.y);
        _leftForceZone.offsetMax =
            new Vector2(-width * _minRequiredForceZoneInPercentages / 100 - _interval, _leftForceZone.offsetMax.y);
    }
}